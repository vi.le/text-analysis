#include <iostream>
#include "freq_analysis.h"

TextAnalysis::TextAnalysis(std::string text) : sanitizedText(text) {
    TextAnalysis::sanitize(this->sanitizedText);
    this->counter = TextAnalysis::numberOfCharacters();
    this->freqs = TextAnalysis::findFreqs(this->sanitizedText);
    this->bigramFreqs = TextAnalysis::findBiGrams(this->sanitizedText);
}

TextAnalysis::TextAnalysis() {}

void TextAnalysis::sanitize(std::string& str) {
    transform(str.begin(), str.end(), str.begin(), ::tolower);
    str.erase(remove_if(str.begin(),str.end(), [](const unsigned char s) { return ! isalnum(s); }), str.end());
}

unsigned int TextAnalysis::numberOfCharacters() {
    return this->sanitizedText.length();
}

std::map<char, double> TextAnalysis::findFreqs(std::string& text) {
    std::map<char, double> freqs;
    for (char& c : text) {
        ++freqs[c];
    }
    return freqs;
}

std::map<std::string, double> TextAnalysis::findBiGrams(std::string& text) {
    std::map<std::string, double> freqsBigram;
    unsigned int i = 0;
    const size_t str_length = text.length();
    for (i = 0; i < text.length(); ++i) {
        if (i < str_length - 1) {
            ++freqsBigram[text.substr(i, 2)];
        }

    }

    return freqsBigram;
}

const std::string TextAnalysis::getText() {
    return this->sanitizedText;
}

unsigned int TextAnalysis::getCounter() {
    return this->counter;
}

const std::map<char, double> TextAnalysis::getEnFreqs() {
    return this->enFreqs;
}
const std::map<char, double> TextAnalysis::getFreqs() {
    return this->freqs;
}

const std::map<std::string, double> TextAnalysis::getBiGrams() {
    return this->bigramFreqs;
}

void TextAnalysis::setTextAnalysis(const std::string& text) {
    this->sanitizedText = text;
    TextAnalysis::sanitize(this->sanitizedText);
    this->counter = TextAnalysis::numberOfCharacters();
    this->freqs = TextAnalysis::findFreqs(this->sanitizedText);
    this->bigramFreqs = TextAnalysis::findBiGrams(this->sanitizedText);
}

double TextAnalysis::indexOfCoincidence() {
    double ic = 0;
    std::map<char, double> freqs = this->freqs;
    for_each(freqs.begin(), freqs.end(), [&ic](const std::pair<char, double>& freq)
            {
            ic += freq.second * (freq.second - 1);
            });
    ic /= this->counter * (this->counter - 1);

    return ic;
}

double TextAnalysis::computeEntropy() {
    double entropy = 0;
    unsigned int counter = this->counter;
    std::map<char, double> freqs = this->freqs;
    for_each(freqs.begin(), freqs.end(), [&entropy, counter](const std::pair<char, double>& freq)
            {
            entropy += (freq.second  / counter) * log2(freq.second / counter);
            });

    return -entropy;
}

double TextAnalysis::computeSinkovStat(const std::string& lang) {
    std::map<char, double> langFreq;
    std::map<char, double> freqs = this->freqs;
    double sinkovStat = 0;
    if (lang == "en") {
        langFreq = this->getEnFreqs();
    }
    unsigned int counter = this->counter;
    for_each(freqs.begin(), freqs.end(), [&sinkovStat, &langFreq, counter](const std::pair<char, double>& freq)
        {
        sinkovStat += freq.second  / counter * langFreq[freq.first];
        });


    return sinkovStat;
}
