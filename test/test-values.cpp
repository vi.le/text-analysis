#include <UnitTest++/UnitTest++.h>
#include <iostream>
#include "../freq_analysis.h"

SUITE(VALUES) {
    class AnalysisFixture {
        public:
            TextAnalysis analysis;

            void init() {
                analysis.setTextAnalysis("In the town where I was born Lived a man who sailed to sea And he told us of his life In the land of submarines So we sailed up to the sun Till we found the sea of green And we lived beneath the waves In our yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine And our friends are all on board Many more of them live next door And the band begins to play We all live in a yellow submarine Yellow submarine, yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine");
            }
    };

    TEST_FIXTURE(AnalysisFixture, IC) {
        init();
        const double ic = analysis.indexOfCoincidence();
        CHECK_EQUAL(0.070935292355936083, ic);
    }

    TEST_FIXTURE(AnalysisFixture, SINKOV) {
        init();
        const double sinkov = analysis.computeSinkovStat("en");
        CHECK_EQUAL(0.064002494758909830, sinkov);
    }

    TEST_FIXTURE(AnalysisFixture, ENTROPY) {
        init();
        const double entropy = analysis.computeEntropy();
        CHECK_EQUAL(3.999270106181698647, entropy);
    }
}

int main(int, const char *[]) {
   return UnitTest::RunAllTests();
}
