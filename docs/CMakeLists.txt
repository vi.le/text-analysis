find_package(Doxygen)
if(DOXYGEN_FOUND)
    set(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/DoxyFile)
    configure_file(${CMAKE_CURRENT_BINARY_DIR}/DoxyFile.in DoxyFile)
    add_custom_target( doc ALL
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation with Doxygen"
        VERBATIM )
else (DOXYGEN_FOUND)
    message("Doxygen needs to be installed to generate the doxygen documentation")
endif(DOXYGEN_FOUND)
