# Text Analysis

A library to analyze text. The goal is to get some statistical informations
about a text :

- Monogram and Bigram frequency counts
- The [Index of Coincidence](https://en.wikipedia.org/wiki/Index_of_coincidence)
- The [Sinkov Statistic](https://en.wikipedia.org/wiki/Sinkov_statistic)
- The [Shannon Entropy](https://en.wiktionary.org/wiki/Shannon_entropy)

## Example :

```c++
#include <iostream>
#include "freq_analysis.h"

int main()
{
    TextAnalysis analysis("In the town where I was born Lived a man who sailed to sea And he told us of his life In the land of submarines So we sailed up to the sun Till we found the sea of green And we lived beneath the waves In our yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine And our friends are all on board Many more of them live next door And the band begins to play We all live in a yellow submarine Yellow submarine, yellow submarine We all live in a yellow submarine Yellow submarine, yellow submarine");
    const double ic = analysis.indexOfCoincidence();
    const double sinkov = analysis.computeSinkovStat("en");
    const double entropy = analysis.computeEntropy();

    std::cout << ic << std::endl;
    std::cout << sinkov << std::endl;
    std::cout << entropy << std::endl;
    return 0;
}
```

Output:

```
0.0709353
0.0640025
3.99927
```

## How to use

```bash
git clone https://github.com/SkySymbol/text-analysis.git && cd text-analysis
cmake .
make
make doc # to build the doxygen documentation
g++ -o main main.cpp -L. -lfreq
```

## Index of coincidence

The index of coincidence measures the probability of picking two letters
randomly and getting the same. The result will be between 0.038466 (random
text) and 0.0686 (English text), if the text was English or a ciphertext
encrypted with a mono-alphabetic substitution method.

## Sinkov statistic

The sinkov statistic measures how likely the text is from a natural language.
A high value indicates that the text is likely to be written in a natural
language.

## Entropy

The entropy measures the amount of information contained in each letter.
