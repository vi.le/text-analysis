#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>

/**
 * @brief A class to analyze text
 */

class TextAnalysis
{
    private:
        /** The text that will be analyzed */
        std::string sanitizedText;

        /** Number of letters in the text */
        unsigned int counter;

        /** The frequency of each letter */
        std::map<char, double> freqs;

        /** The bi-gram frequencies */
        std::map<std::string, double> bigramFreqs;

        /** The frequency of letters in the English language */
        const std::map<char, double> enFreqs = {
            { 'a', 0.08167 },
            { 'b', 0.01492 },
            { 'c', 0.02782 },
            { 'd', 0.04253 },
            { 'e', 0.12702 },
            { 'f', 0.02228 },
            { 'g', 0.02015 },
            { 'h', 0.06094 },
            { 'i', 0.06966 },
            { 'j', 0.00153 },
            { 'k', 0.00772 },
            { 'l', 0.04025 },
            { 'm', 0.02406 },
            { 'n', 0.06749 },
            { 'o', 0.07507 },
            { 'p', 0.01929 },
            { 'q', 0.00095 },
            { 'r', 0.05987 },
            { 's', 0.06327 },
            { 't', 0.09056 },
            { 'u', 0.02758 },
            { 'v', 0.0978  },
            { 'w', 0.02360 },
            { 'x', 0.00150 },
            { 'y', 0.01974 },
            { 'z', 0.00074 }
        };

        /**
         * @returns the number of characters in @ref sanitizedText
         */
        unsigned int numberOfCharacters();

        /**
         * Function to remove every non-alphanumeric characters in the text
          @param[out] text to be sanitized
         */
        void sanitize(std::string& text);

        /**
         * Find the frequencies of letters in the text
         * @param text The text
         * @return The number of occurrence for each character
        */
        std::map<char, double> findFreqs(std::string& text);

        /**
         * Find the frequencies of bigrams in the text
         * @param text The text
         * @return The number of occurrence of each bigram found
        */
        std::map<std::string, double> findBiGrams(std::string& text);

        /**
         * Get the frequency of the letters in english
         * Taken from https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_the_English_language
         * @return The frequency of letters in the English language
        */
        const std::map<char, double> getEnFreqs();


    public:
        /**
         * Constructor for the @ref TextAnalysis class
         *
         * Set the private members of the class
         * @param text A string
         */
        explicit TextAnalysis(std::string text);

        /**
         * Default constructor
         */
        TextAnalysis();


        /**
         * To get the text that is analyzed
         * @return @ref sanitizedText
         */
        const std::string getText();

        /**
         * To get the number of characters in the text
         * @return @ref counter
         */
        unsigned int getCounter();

        /**
         * To get the frequencies of each letter
         * @return @ref freqs
         */
        const std::map<char, double> getFreqs();

        /**
         * To get the frequencies of each bigram
         * @return @ref bigramFreqs
         */
        const std::map<std::string, double> getBiGrams();

        /**
         * To set the text to be analyzed
         * @param text A string
         */
        void setTextAnalysis(const std::string& text);

        /**
         * Compute the <a href="https://en.wikipedia.org/wiki/Index_of_coincidence">Index of Coincidence</a>
         * @return The indice of coincidence
         */
        double indexOfCoincidence();

        /**
         * Compute the <a href="https://en.wikipedia.org/wiki/Entropy">Entropy</a> 
         * @return The entropy
         */
        double computeEntropy();

        /**
         * Compute the <a href="https://en.wikipedia.org/wiki/Sinkov_statistic">Sinkov statistic</a> 
         * @param a @ref A language in <a href="https://en.wikipedia.org/wiki/ISO_639-1">ISO 639-1 format</a>
         * @return The Sinkov statistic
         */
        double computeSinkovStat(const std::string& lang);
};

